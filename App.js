const puppeteer = require('puppeteer-core');
const config = require('./src/config/config');
const auth = require('./src/auth');
const init = require('./src/init');
const addToCart = require('./src/cart/atc');
const checkout = require('./src/cart/co');
const pay = require('./src/cart/pay');
const timer = require('./src/time/timeout');
const product = require('./src/config/product');

const App = async () => {
    const browser = await puppeteer.launch(config);

    try {

        // check login state
        await auth(browser);

        // init
        const pages = await init(browser);

        // timer
        const timerRes = await timer('09:45:40');
        console.log(timerRes);

        // reload product
        console.time('reload-product');
        await pages[0].goto(product.link, { waitUntil: 'domcontentloaded' });
        console.timeEnd('reload-product');

        console.time('order')
        // add to cart
        await addToCart(pages[0], product.link);
        console.log('added to cart')

        // checkout
        await checkout(pages[1]);

        // payment
        await pay(pages[1]);
        console.timeEnd('order')

        await pages[1].screenshot({path: './public/pay.png', fullPage: true});

        await browser.close();

    } catch(err) {
        await browser.close(); // close the browser
        console.log(err);
    }

}

App();
