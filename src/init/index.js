const product = require('./../config/product');

const init = async (browser) => {
    return new Promise(async (resolve, reject) => {

        try {
            // describe page
            const productP = await browser.newPage();
            const cartP = await browser.newPage();

            // goto page
            console.time('init-load')
            await productP.goto(product.link, { waitUntil: 'domcontentloaded' });
            await cartP.goto(product.cart, { waitUntil: 'domcontentloaded' });
            console.timeEnd('init-load')

            // console.time('init-load')
            // await productP.goto(product.link, { waitUntil: 'domcontentloaded' });
            // await cartP.goto(product.cart, { waitUntil: 'domcontentloaded' });
            // console.timeEnd('init-load')

            let res = [ productP, cartP];
            resolve(res);
        } catch(err) {
            console.log(err);
            reject(err);
        }
    });
}

module.exports = init;
