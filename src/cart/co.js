const product = require('./../config/product');
const targetName = product.name;

const co = async (page) => {
    return new Promise(async (resolve, reject) => {
        try {

            // reload the cart
            console.time('reload-cart');
            await page.goto('https://shopee.co.id/cart', { waitUntil: 'domcontentloaded' });
            console.timeEnd('reload-cart');

            // waiting product presented
            console.time('waiting-cb');
            await page.evaluate(() => {
                return new Promise((res, rej) => {
                    let cb;

                    let finding = setInterval(() => {
                        cb = document.querySelector('.stardust-checkbox__input');

                        if (cb != null) {
                            res(true);
                            clearInterval(finding);
                        }
                    });
                });
            });
            console.timeEnd('waiting-cb');

            // finding product
            console.time('finding-product');
            await page.evaluate((targetName) => {
                return new Promise((res, rej) => {
                    var productCart = document.querySelectorAll('._17hSZB');
                    var productCartSize = productCart.length;
                    var targetNameConv = targetName.toLowerCase().replace(/[^a-z]/g, '');

                    for (let i = 0; i < productCartSize; i++) {

                        let productName = productCart[i].querySelector('._3OP3bk').textContent.toLowerCase().replace(/[^a-z]/g, '');
                        if (productName == targetNameConv) {
                            productCart[i].querySelector('.stardust-checkbox__input').click();
                            console.log(productName)
                            res(true)
                            break;
                        }
                    }
                });
            }, targetName);
            console.timeEnd('finding-product');


            // click checkout button
            console.log('clicking checkout button');
            await page.evaluate(() => {
                document.querySelector('.stardust-button--primary').click();
            });

            // wait pay button
            console.time('waiting-pay')
            await page.waitForSelector('.page-checkout-place-order-section__button')
            console.timeEnd('waiting-pay')

            // resolve
            resolve('done');
        } catch(err) {
            console.log(err);
            reject(err);
        }
    });
}

module.exports = co;