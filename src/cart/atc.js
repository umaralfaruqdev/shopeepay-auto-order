const waitAtc = require('./wait-atc');

const atc = async (page, link) => {
    return new Promise(async (resolve, reject) => {
        try {

            // finding atc button
            console.log("finding atc button");
            const find1 = await waitAtc(page);
            console.log(find1);

            // if after reload not detect cart button
            // then reload
            let find2;
            if (!find1) {
                console.log("reload-product-2")
                await page.goto(link, { waitUntil: 'domcontentloaded' });
                find2 = await waitAtc(page);
            } else {
                // set to true if find1 ok
                find2 = true;
            }
            console.log(find2);

            // if after reload not detect cart button
            // then reload
            let find3;
            if (!find2) {
                console.log("reload-product-3")
                await page.goto(link, { waitUntil: 'domcontentloaded' });
                find3 = await waitAtc(page);
            }
            console.log(find3); 

            console.time('finding-toast');
            let atcRes = await page.evaluate(() => {
                document.querySelector('.product-bottom-panel__add-to-cart').click();

                return new Promise((res, rej) => {
                    var stard;
                    var finding = setInterval(function () {
                        stard = document.querySelector('.stardust-toast__text');
                        if (stard != null) {
                            clearInterval(finding);
                            res(stard.textContent);
                        } else {
                            console.log(stard);
                        }
                    }, 100);
                });
            });

            console.log(atcRes); // log toast text
            
            console.timeEnd('finding-toast');

            resolve('product aded');
        } catch(err) {
            reject(err);
        }
    });
}

module.exports = atc;
