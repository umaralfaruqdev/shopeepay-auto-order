const waitAtc = async (page) => {
    return new Promise(async (resolve, reject) => {

        // find atc button
        console.time('finding-atc');
        const findingAtc = await page.evaluate(() => {
            return new Promise((res, rej) => {
                let atcBtn;
                let soonBtn;

                var finding = setInterval(function () {
                    atcBtn = document.querySelector('.product-bottom-panel__add-to-cart');
                    soonBtn = document.querySelector('.product-bottom-panel__coming-soon-bar');

                    if (atcBtn != null) {
                        res(true);
                        clearInterval(finding);
                    } else if(soonBtn != null) {
                        res(false);
                        clearInterval(finding);
                    }
                });
            });
        });
        console.timeEnd('finding-atc');

        // resolving
        resolve(findingAtc);
    });
}

module.exports = waitAtc;