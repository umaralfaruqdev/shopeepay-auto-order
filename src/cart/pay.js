const pay = async (page) => {
    return new Promise(async (resolve, reject) => {
        try {

            console.log('waiting ready for pay');
            console.time('waiting-ready-pay')
            await page.evaluate(() => {
                return new Promise((res, rej) => {
                    let price;
                    let payBtn;
                    let waitLimit = 15000;
                    let waitCount = 0;

                    let finding = setInterval(() => {
                        price = document.querySelector('.page-checkout-total-payment__price');
                        waitCount += 100;
                        console.log(waitCount);

                        if (waitCount >= waitLimit) {
                            rej('waiting price content is timeout 15000m');
                            clearInterval(finding);
                        }

                        if (price.textContent != 'Rp') {
                            payBtn = document.querySelector('.page-checkout-place-order-section__button');
                            // payBtn.click();
                            res('pay found');
                            clearInterval(finding);
                        }
                    }, 100);
                });
            });
            console.timeEnd('waiting-ready-pay')

            // resolve
            resolve('done');
        } catch(err) {
            await page.screenshot({path: './public/error-waiting-ready-pay.png', fullPage: true}); // screenshot
            reject(err);
        }
    });
}

module.exports = pay;