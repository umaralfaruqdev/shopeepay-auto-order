const checkAuth = require('./check-login');
const doLogin = require('./do-login');

const auth = async (browser) => {
    return new Promise(async (resolve, reject) => {
        const { state, page } = await checkAuth(browser); // check auth state
        
        if (!state) {
            // if not loged in
            console.log('do login...')
            const loginResponse = await doLogin(page);
            await page.close(); // close page

            console.log(loginResponse);
            resolve(true);
        } else {
            console.log('already login');
            resolve(true);
        }
    });
}

module.exports = auth;