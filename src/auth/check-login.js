const checkLogin = async (browser) => {
    return new Promise(async (resolve, reject) => {
        try {
            let loginPage = await browser.newPage();
            let url = 'https://shopee.co.id/api/v1/account_info/?from_wallet=true&need_cart=1&skip_address=1';

            await loginPage.goto(url, { waitUntil: 'domcontentloaded' });
            
            // get content
            const content = await loginPage.evaluate(() => {
                // parse to json object
                return JSON.parse(document.querySelector('body').innerText);
            });

            if (Object.keys(content).length === 0) {
                // if content is empty
                let res = { page: loginPage, state: false };
                resolve(res);
            } else {
                // if already logedin
                let res = { page: null, state: true };
                loginPage.close(); // close the page
                resolve(res);
            }

        } catch(err) {
            console.log(err);
        }
    });
}

module.exports = checkLogin;
