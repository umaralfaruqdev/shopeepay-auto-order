const cred = require('./../config/cred');

const doLogin = async (page) => {
    return new Promise(async (resolve, reject) => {
        let url = 'http://shopee.co.id/buyer/login';
        await page.goto(url, { waitUntil: 'domcontentloaded' });

        // waiting selector
        await page.waitForSelector('._1kUCZd');

        // define elements
        const userEl = await page.evaluateHandle(() => document.querySelector('._1kUCZd'));
        const passEl = await page.evaluateHandle(() => document.querySelectorAll('._1kUCZd')[1]);

        await userEl.type(cred.user); // fill username
        await passEl.type(cred.pass); // fill password

        // submit button
        await page.evaluate(() => {
            return new Promise((resolve, reject) => {
                let targetEl;
                let search = setInterval(() => {
                    targetEl = document.querySelector('._20cPBy');

                    // if button 
                    if (targetEl.getAttribute('disabled') == null) {
                        resolve('finded');
                        document.querySelector('._20cPBy').click(); // submit
                        clearInterval(search);
                    }
                }, 100);
            });
        });

        // detect success login
        await page.waitForSelector('.avatar');
        resolve('login success');
    });
}

module.exports = doLogin;