var moment = require('moment-timezone');


const timer = (time) => {

    return new Promise((res, rej) => {
        let getWaktu = setInterval(() => {
            if (moment().tz("Asia/Jakarta").format('hh:mm:ss') == time) {
                res('starting order');
                clearInterval(getWaktu);
            } else {
                console.log(moment().tz("Asia/Jakarta").format('hh:mm:ss'))
            }
        }, 100);
    });
}

module.exports = timer;