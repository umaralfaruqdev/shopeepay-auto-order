const args = [
    '--no-sandbox',
    '--disable-setuid-sandbox',
    '--disable-infobars',
    '--ignore-certifcate-errors',
    '--ignore-certifcate-errors-spki-list',
    '--user-agent="Mozilla/5.0 (Android 8.1.0; Mobile; rv:61.0) Gecko/61.0 Firefox/61.0"',
];

let config = {
    headless: false,
    userDataDir: '/tmp/wd',
    ignoreHTTPSErrors: true,
    args: args,
    ignoreDefaultArgs: ['--enable-automation'],
    executablePath: '/opt/google/chrome/chrome',
}

module.exports = config;
